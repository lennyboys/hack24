import React from 'react';
import ReactDom from 'react-dom';
import {browserHistory, Router, Route, IndexRoute} from 'react-router';

import Template from './components/template';
import Home from './components/home';
import NotFound from './components/not-found';
import HelloWorldGame from './components/hello-world-game';
import LennyRunGame from './components/lenny-run';
import FlappyLennyGame from './components/flappy-lenny';
import Tinder from './components/tinder';

export default function() {
  ReactDom.render(
    <Router history={browserHistory}>
      <Route path='/' component={Template}>
        <IndexRoute component={Home} />
        <Route path="hello-world" component={HelloWorldGame} />
        <Route path="lenny-run" component={LennyRunGame} />
        <Route path="flappy-lenny" component={FlappyLennyGame}/>
        <Route path="tinder" component={Tinder} />
        <Route path="*" component={NotFound} />
      </Route>
    </Router>,
    document.getElementById('root'))
}
var express = require('express');
var request = require('request');
var app = express();
var destination = "http://lenny.today";

app.get('/*', (req, res, next) => {
  var url = destination + req.url;

  request(url, (e, r, b) => {
    res.send(b);
  });
});

app.listen(3001, () => {
  console.log("API listening on 3001");
});
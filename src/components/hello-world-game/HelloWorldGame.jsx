import React from 'react';
import GameEngine from '../game-engine';

class Game extends GameEngine {
  setInitialState() {
    return {
      n: 0,
      x: 0,
      y: 0
    }
  }

  draw(canvas) {
    canvas.fillStyle = "black";
    canvas.fillText("Hello " + this.state.n, 100 + this.state.x, 100 + this.state.y);
  }

  update() {
    this.setState({
      n : (this.state.n || 0) + 1
    });
  }

  onKeyDown(key) {
    if (key.key === "w") {
      this.setState({
        y: this.state.y - 10
      });
    }

    if (key.key === "s") {
      this.setState({
        y: this.state.y + 10
      });
    }

    if (key.key === "a") {
      this.setState({
        x: this.state.x - 10
      });
    }

    if (key.key === "d") {
      this.setState({
        x: this.state.x + 10
      });
    }
  }
}

class HelloWorldGame extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello World Game</h1>
        <Game width={750} height={400} />
      </div>
    )
  }
}

export default HelloWorldGame;
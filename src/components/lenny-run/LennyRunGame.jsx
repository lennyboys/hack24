import React from 'react';
import GameEngine from '../game-engine';

class Game extends GameEngine {

  setInitialState() {
    fetch("/api/v1/random?limit=500")
      .then(res => res.json())
      .then(j => {
        this.setState({
          lennies: j.map(f => f.face)
        });
      })
      .catch(err => console.error(err));

    return {     
      winner: false,
      started: false,

      progress1Trigger: (Math.random() * 2000) + 250,
      progress2Trigger: (Math.random() * 2000) + 2250,
      progress3Trigger: (Math.random() * 2000) + 4250,
      progress4Trigger: (Math.random() * 2000) + 6250,
      progress5Trigger: (Math.random() * 2000) + 7000,

      startTime: new Date(),
      finishTime: undefined,
      distanceRan: 0,
      distanceToRun: 10000,
      lastKeyPress: 0,
      speed: 0,
      acceleration: 0,
      maxSpeed: 10,
      maxAcceleration: 2,
      slowdownRate: 0.05,
      doSlowdown: false,
      treeDistances: [150, 225, 100, 75, 300, 100, 450],
      lennies: [],
      progressMessages: ["Keep Going!", "Don't Stop Now!", "You Can Do It!", "Just a little further!",
                              "( ͡° ͜ʖ ͡°)", "You are great!", "I love you Lenny!", "Believe in yourself",
                              "You're doing SO WELL!"],

      progress1Message: (Math.floor(Math.random() * 9)),
      progress2Message: (Math.floor(Math.random() * 9)),
      progress3Message: (Math.floor(Math.random() * 9)),
      progress4Message: (Math.floor(Math.random() * 9)),
      progress5Message: (Math.floor(Math.random() * 9))           
    }
  }

  draw(canvas) {
    this.drawBackground(canvas);
    this.drawSun(canvas);
    this.drawCrowd(canvas);
    this.drawTrees(canvas);
    this.drawTrack(canvas);
    this.drawLenny(canvas);
    this.drawBorder(canvas);
    if(this.state.started) this.drawHUD(canvas);
  }

  drawBackground(canvas){
    canvas.fillStyle = "#A2CAFF";
    canvas.fillRect(5,5, this.props.width - 5, this.props.height - 150);

    canvas.fillStyle = "#AFA";
    canvas.fillRect(5, 105, this.props.width - 5, this.props.height - 5);
  }

  drawBorder(canvas){
    canvas.fillStyle = "black";
    canvas.fillRect(0, 0, this.props.width, 5);
    canvas.fillRect(0, 0, 5, this.props.height);
    canvas.fillRect(this.props.width - 5, 0, 5, this.props.height);
    canvas.fillRect(0, this.props.height - 5, this.props.width, 5);
  }

  drawTrees(canvas){
    var treeDistances = [300, 450, 450, 550, 325, 475];
    var magicTreeSceneWidthPadding = 1300;
    var treeSceneWidth = treeDistances.reduce((a, b) => a + b, 0) + this.props.width + magicTreeSceneWidthPadding;
    var xOffset = -(this.state.distanceRan % treeSceneWidth)*0.75 + this.props.width;

    var x = 50 + xOffset;
    var y = this.props.height - 175;
    this.drawTree(canvas, x, y);

    for(var i = 0; i < treeDistances.length; i++){
      x += treeDistances[i];
      if(x < -50 || x > this.props.width + 50){
        continue;
      }
      this.drawTree(canvas, x, y);
    }
  }

  drawTree(canvas, x, y){
    var trunkWidth = 10;
    var trunkHeight = 50;
    var leafRadius = trunkWidth * 2.5;

    canvas.fillStyle = "brown";
    canvas.fillRect(x - trunkWidth / 2, y, trunkWidth, trunkHeight);

    canvas.beginPath();
    canvas.fillStyle = "green";
    canvas.arc(x, y - leafRadius, leafRadius, 0, Math.PI * 2);
    canvas.fill();
  }

  drawCrowd(canvas){
    var crowdStandWidth = this.props.width * 2;
    var maxXOffset = crowdStandWidth - this.props.width;
    var percentageOfOffset = Math.min(1, (this.state.distanceRan / (this.state.distanceToRun + 1500)));
    var xOffset = -percentageOfOffset * maxXOffset;

    // Stand back
    canvas.fillStyle = "orange";
    canvas.fillRect(0, 125, this.props.width, 75);

    // Stand top border
    canvas.fillStyle = "brown";
    canvas.fillRect(0, 125, this.props.width, 5);

    // Crowd members
    if(this.state.lennies.length > 0) {
      for(var i = 0; i < crowdStandWidth / 50; i++){
        var x = 10 + (i * 50) + xOffset;

        if(x < 0 || x > this.props.width + 50){
          continue;
        }

        var lennyIndeces = [i*3 % (this.state.lennies.length), 
                            (i*3+1) % (this.state.lennies.length),
                            (i*3+2) % (this.state.lennies.length)];

        this.drawCrowdMember(canvas, this.state.lennies[lennyIndeces[0]], x, 144);
        this.drawCrowdMember(canvas, this.state.lennies[lennyIndeces[1]], x + 10, 160);
        this.drawCrowdMember(canvas, this.state.lennies[lennyIndeces[2]], x + 4, 176);
      }
    }

    // Stand bottom border
    canvas.fillStyle = "brown";
    canvas.fillRect(0 + xOffset, 185, crowdStandWidth, 15);
  }

  drawCrowdMember(canvas, lennyFace, x, y){
    canvas.textAlign = "right";
    canvas.fillStyle = "black";
    canvas.font = "10px sans-serif";
    canvas.fillText(lennyFace, x, y);
  }

  drawSun(canvas){
    canvas.beginPath();
    canvas.fillStyle = "yellow";
    canvas.arc(this.props.width - 80, 50, 30,  0, Math.PI * 2);
    canvas.fill();

  }

  drawTrack(canvas){
    canvas.fillStyle = "orange";
    canvas.fillRect(5, this.props.height - 100, this.props.width - 10, 75);
    canvas.fillStyle = "brown";

    var lineWidth = 5;
    var lineSpacing = 50;
    var xOffset = -this.state.distanceRan % (lineWidth + lineSpacing);
    for(var i = 0; i < this.props.width / (lineWidth + lineSpacing) + (lineWidth + lineSpacing); i++){
      canvas.fillRect(5 + i * (lineWidth + lineSpacing) + xOffset, this.props.height - 100, 5, 75);
    }
  }

  drawLenny(canvas){
    canvas.textAlign = "left";
    canvas.fillStyle = "black";
    canvas.font = "30px sans-serif";
    canvas.fillText("( ͡° ͜ʖ ͡°)", this.props.width / 2 - 30, this.props.height - 110);
  }

  drawHUD(canvas){
    canvas.fillStyle = "black";
    canvas.font = "20px sans-serif";
    canvas.textAlign = "left";
    canvas.fillText("Time Taken: " + ((this.state.endTime - this.state.startTime) / 1000) + " secs", 20, 35);
    canvas.textAlign = "right";
    canvas.fillText("Distance: " + Math.floor(this.state.distanceRan), this.props.width - 15, 35)

    var pickedValue = "";

    if(this.state.distanceRan > this.state.progress1Trigger && this.state.distanceRan < this.state.progress1Trigger + 1000)
    {
      pickedValue = this.state.progressMessages[this.state.progress1Message];
      canvas.fillText(pickedValue, 300, 100);
    }
    if(this.state.distanceRan > this.state.progress2Trigger && this.state.distanceRan < this.state.progress2Trigger + 1000)
    {
      pickedValue = this.state.progressMessages[this.state.progress2Message];
      canvas.fillText(pickedValue, 400, 100);
    }
    if(this.state.distanceRan > this.state.progress3Trigger && this.state.distanceRan < this.state.progress3Trigger + 1000)
    {
      pickedValue = this.state.progressMessages[this.state.progress3Message];
      canvas.fillText(pickedValue, 600, 100);
    }
    if(this.state.distanceRan > this.state.progress4Trigger && this.state.distanceRan < this.state.progress4Trigger + 1000)
    {
      pickedValue = this.state.progressMessages[this.state.progress4Message];
      canvas.fillText(pickedValue, 400, 100);
    }
    if(this.state.distanceRan > this.state.progress5Trigger && this.state.distanceRan < this.state.progress5Trigger + 1000)
    {
      pickedValue = this.state.progressMessages[this.state.progress5Message];
      canvas.fillText(pickedValue, 200, 100);
    }
    if(this.state.winner){
      canvas.fillText("U R WINNAR", (this.props.width / 2 - 100), 100);
    }
  }

  update() {
    this.setState({
      distanceRan: this.state.distanceRan + this.state.speed,
      speed: Math.max(0, this.state.speed - this.state.slowdownRate)
    });

    if(this.state.started){
      if(this.state.distanceRan > this.state.distanceToRun && !this.state.winner){
        this.setState({
          endTime: new Date(),
          winner: true
        });
      }
      else if(!this.state.winner) {
        this.setState({
          endTime: new Date()
        });
      }
    }
  }

  onKeyUp(event){
    if (event.key === " " && !this.state.winner) {
      event.preventDefault();
      this.increaseSpeed();
    }
  }

  onClick(key) {
    if (key.buttons === 0 && !this.state.winner) {
      this.increaseSpeed();
    }
  }

  increaseSpeed(){
    if(!this.state.started){
      this.setState({
        startTime: new Date(),
        started: true
      });
    }

    this.setState({
      speed: Math.min(this.state.maxSpeed, this.state.speed + 1)
    });
  }
}

class LennyRunGame extends React.Component {
  render() {
    return (
      <div>
        <h1>Lenny Run</h1>
        <Game width={750} height={400} />
      </div>
    )
  }
}

export default LennyRunGame;
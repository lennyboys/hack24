import React from 'react';
import GameEngine from '../game-engine';

class Game extends GameEngine {
  draw(canvas) {
    canvas.fillStyle = "black";
    canvas.fillText("Hello " + this.state.n, 10, 10);
  }

  update() {
    this.setState({
      n : (this.state.n || 0) + 1
    });
  }
}

class FlappyLennyGame extends React.Component {
  render() {
    return (
      <div>
        <h1>Flappy Lenny</h1>
        <Game width={750} height={400} />
      </div>
    )
  }
}

export default FlappyLennyGame;
import React from 'react';
import "./Tinder.css";

class Preview extends React.Component {
  render() {
    return (
      <div className={"preview " + 
      (this.props.like ? "swipe-right" : "") + 
      (this.props.dislike ? "swipe-left" : "")}>
        {this.props.lenny}
      </div>
    )
  }
}

class Tinder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lennies: ["( ͡° ͜ʖ ͡°)"],
      lennyLike: undefined,
      lennyDislike: undefined,
      random: 0
    };
  }

  componentDidMount() {
    this.setState({
      lennies: ["( ͡° ͜ʖ ͡°)"]
    })
    this.update();
  }

  render() {
    console.log(this.state.lennyLike);
    return (
      <div className="tinder-container">
        <div className="preview-container">
          {this.state.lennyDislike ? <Preview key={this.state.random+1} lenny={this.state.lennyDislike} dislike={true} /> : null}
          {this.state.lennyLike ? <Preview key={this.state.random} lenny={this.state.lennyLike} like={true} /> : null}
          {this.state.lennies.map((l, k) => <Preview key={k} lenny={l} />).reverse()}
        </div>

        <div className="options-container">
          <div className="dislike" onClick={this.dislike.bind(this)}>x</div>
          <div className="like" onClick={this.like.bind(this)}>y</div>
        </div>
      </div>
    )
  }

  dislike() {
    this.setState({
      lennies: this.state.lennies.slice(1),
      lennyLike: undefined,
      lennyDislike: this.state.lennies[0],
      random: Math.random()
    });
    this.update();
  }

  like() {
    this.setState({
      lennies: this.state.lennies.slice(1),
      lennyDislike: undefined,
      lennyLike: this.state.lennies[0],
      random: Math.random()
    });
    this.update();
  }

  update() {
    if (this.state.lennies.length < 10) {
      fetch("/api/v1/random?limit=20")
      .then(res => res.json())
      .then(j => this.setState({
        lennies: [...this.state.lennies, ...(j.map(f => f.face))]
      }))
      .catch(err => console.error(err));
    }
  }
}

export default Tinder;
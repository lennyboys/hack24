import React from 'react';

class GameEngine extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {

    // Register game loop
    setInterval(() => {
      const canvas = document.getElementById("canvas").getContext("2d");
      this.initSetup();
      this.initDraw(canvas);
      this.update();
      this.draw(canvas);
    }, 1000/60);

    const canvas = document.getElementById("canvas");

    // Register key events
    if (this.onKeyDown) canvas.addEventListener("keydown", this.onKeyDown.bind(this), true);
    if (this.onKeyUp) canvas.addEventListener("keyup", this.onKeyUp.bind(this), true);
    if (this.onKeyPress) canvas.addEventListener("keypress", this.onKeyPress.bind(this), true);

    // Register mouse events
    if (this.onClick) canvas.addEventListener("click", this.onClick.bind(this), true);

    this.setState(this.setInitialState() || {});
  }

  /*
  Methods for subclasses

  update = () => { };
  draw = (canvas) => { };
  onKeyDown = (key) => { };
  onKeyUp = (key) => { };
  onKeyPress = (key) => { };
  onClick = (mouse) => { };
  */


  render() {
    return (
      <canvas id="canvas" tabIndex={1} width={this.props.width} height={this.props.height} />
    );
  }

  initDraw(canvas) {
    canvas.clearRect(0, 0, this.props.width, this.props.height);
  }

  initSetup() {

  }

  static propTypes = {
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired
  }
}

class ExampleChild extends GameEngine {
  update() {

  }

  draw(canvas) {

  }
}

export default GameEngine;